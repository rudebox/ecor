<?php 
/**
* Description: Lionlab business pricetabel repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$sub_title = get_sub_field('subheader');

if (have_rows('pricetabel_box') ) :
?>

<section class="pricetabel <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad flex flex--wrap pricetabel__container">
		<?php if ($title) : ?>
			<h2 class="pricetabel__heading center"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<?php if ($sub_title) : ?>
			<div class="pricetabel__heading--subheader center col-sm-8 col-sm-offset-2"><?php echo esc_html($sub_title); ?></div>
		<?php endif; ?>
		
		<div class="pricetabel__calc center">
			<strong>Månedlig betaling/Årlig betaling</strong>
			<div class="pricetabel__btn">
				<label class="pricetabel__btn--label"></label>
				<input id="js-btn-toggle" class="btn btn--toggle" type="checkbox">
			</div>
		</div>

		<div class="row flex flex--wrap">
			<?php 
				while (have_rows('pricetabel_box') ) : the_row();

					$title = get_sub_field('pricetabel_title');
					$sub_title = get_sub_field('pricetabel_subtitle');
					$text = get_sub_field('pricetabel_text');
					$price = get_sub_field('pricetabel_price');
					$featured = get_sub_field('pricetabel_featured');
					$phone = get_sub_field('pricetabel_phone');

					if ($featured === true ) {
						$class = 'pricetabel__item--featured';
						$attr = '';
					}

					else {
						$class = '';
						$attr = 'data-aos-delay=100';
					}
			 ?>

			 <div <?php echo esc_attr($attr); ?> data-aos="zoom-in" class="col-sm-4 pricetabel__item <?php echo esc_attr($class); ?> center">
			 	<div class="pricetabel__header">
					<h2 class="h3 pricetabel__title"><?php echo esc_html($title); ?></h2>
				 	<?php echo esc_html($sub_title); ?>
			 	</div>
			 	<div class="pricetabel__content"><?php echo $text; ?></div>
				
				<?php if ($featured === true) : ?>
			 		<div class="pricetabel__phone"><p>Ring på tlf.<p><?php echo esc_html($phone); ?></div>
			 	<?php endif; ?>

			 	<p class="pricetabel__price pricetabel__price--monthly is-active"><?php echo esc_html($price); ?>,- <span>/md</span></p>
			 	<p class="pricetabel__price pricetabel__price--proanno is-hidden"><?php echo esc_html($price * 12); ?>,- <span>/pr. år.</span></p>
			 </div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>