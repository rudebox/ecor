<?php

/*
 * Template Name: Layouts
 */
 ?>

<?php get_template_part('parts/header'); the_post(); ?>

<main class="transition__body"> 


	<?php get_template_part('parts/page', 'header');?>

	<?php get_template_part('parts/content', 'layouts'); ?>

	<?php 
		$location = get_sub_field('google_maps');

		if ( have_rows('locations') ) :
			get_template_part('parts/google', 'maps');
		endif;
	?>

	<?php get_template_part('parts/newsletter'); ?>  


</main>

<?php get_template_part('parts/footer'); ?>
