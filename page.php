<?php get_template_part('parts/header'); the_post(); ?>

<main class="transition__body"> 

    <?php get_template_part('parts/page', 'header');?>

    <?php get_template_part('parts/content', 'layouts'); ?>

    <?php get_template_part('parts/newsletter'); ?> 

</main>

<?php get_template_part('parts/footer'); ?>