<?php 
/**
* Description: Lionlab employee repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('employee') ) :
?>

<section class="employee padding--<?php echo esc_html($margin); ?> <?php echo esc_html($bg); ?>--bg">
	<div class="wrap hpad">
		<h2 class="employee__header"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap">
			<?php 
				while (have_rows('employee') ) : the_row();

					$img = get_sub_field('employee_img');
					$name = get_sub_field('employee_name');
					$position = get_sub_field('employee_position');
					$phone = get_sub_field('employee_phone');
					$mail = get_sub_field('employee_mail');
					$linkedin = get_sub_field('employee_linkedin');

 			 ?>

 			 <div data-aos="fade-in" class="col-sm-4 employee__item">
 			 	<div class="employee__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
 			 	
 			 	</div>
 			 	<div class="employee__content">
	 			 	<h5 class="employee__name"><?php echo esc_html($name); ?></h5>
	 			 	<p class="employee__position"><?php echo esc_html($position); ?></p>
	 			 	<?php if ($linkedin) : ?>
	 			 	<a class="employee__link" target="_blank" href="<?php echo esc_url($linkedin); ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
	 			 	<?php endif; ?>
	 			 	<a class="employee__link" href="tel:<?php echo esc_html(get_formatted_phone($phone)); ?>"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo esc_html($phone); ?></a>
	 			 	<a class="employee__link" href="mailto:<?php echo esc_html($mail); ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo esc_html($mail); ?></a>
 			 	</div>
 			 </div>

 			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>
