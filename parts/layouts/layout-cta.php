<?php 
/**
* Description: Lionlab cta field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

//cta settings
$img = get_sub_field('cta_img');
$title = get_sub_field('cta_title');
$text = get_sub_field('cta_text');
$link = get_sub_field('cta_link');
$link_text = get_sub_field('cta_link_text');
?>

<section class="cta <?php echo $bg; ?>--bg" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
	<div class="wrap hpad">
		<div class="row">
			
			<div class="cta__item col-sm-8 col-sm-offset-2">
				<h2 class="cta__title"><?php echo esc_html($title); ?></h2>
				<div class="cta__text"><?php echo esc_html($text); ?></div>
				<?php if ($link) : ?>
				<a class="btn btn--green cta__btn" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
				<?php endif; ?>
			</div>
			
		</div>
	</div>
</section>