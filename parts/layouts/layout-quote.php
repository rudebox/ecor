<?php 
/**
* Description: Lionlab quote field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

if (have_rows('quote_box') ) :
?>

<section data-aos="fade-in" class="quote padding--<?php echo $margin; ?> <?php echo $bg; ?>--bg">
  <div class="wrap hpad">
    <div class="row flex flex--wrap">

      <?php while (have_rows('quote_box') ) : the_row(); 
			//field group
			$title = get_sub_field('quote_title');
			$text = get_sub_field('quote_text');
      ?>

	      <div class="col-sm-6 quote__item">
	        <h2 class="quote__title h4"><?php echo esc_html($title); ?></h2>
	        <em class="quote__text"><i class="fas fa-quote-right"></i><?php echo $text; ?></em>
	      </div>
  	  <?php endwhile; ?>

    </div>
  </div>
</section>
<?php endif; ?>
