<?php 
/**
* Description: Lionlab newsletter field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/


$img = get_field('news_img', 'options');
$title = get_field('news_title', 'options');
?>

<section class="news" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
	<div class="wrap hpad">
		<div class="row">
			
			<div class="news__item col-sm-8 col-sm-offset-2 center">
				<h2 class="news__title"><?php echo esc_html($title); ?></h2> 
				<?php echo do_shortcode('[mc4wp_form id="94"]'); ?>
			</div>
			
		</div>
	</div>
</section>