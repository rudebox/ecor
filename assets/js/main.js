jQuery(document).ready(function($) {


  /*
   *  render_map
   *
   *  This function will render a Google Map onto the selected jQuery element
   *
   *  @type  function
   *  @date  8/11/2013
   *  @since 4.3.0
   *
   *  @param $el (jQuery element)
   *  @return  n/a
   */

  function render_map($el) {

    // var
    var $markers = $el.find('.marker');

    // vars
    var args = {
      zoom: 16,
      center: new google.maps.LatLng(0, 0),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles : [
          {
              "featureType": "water",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#061f28"
                  },
                  {
                      "lightness": 0
                  }
              ]
          },
          {
              "featureType": "landscape",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#f5f5f5"
                  },
                  {
                      "lightness": 20
                  }
              ]
          },
          {
              "featureType": "road.highway",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "#ffffff"
                  },
                  {
                      "lightness": 17
                  }
              ]
          },
          {
              "featureType": "road.highway",
              "elementType": "geometry.stroke",
              "stylers": [
                  {
                      "color": "#ffffff"
                  },
                  {
                      "lightness": 29
                  },
                  {
                      "weight": 0.2
                  }
              ]
          },
          {
              "featureType": "road.arterial",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#ffffff"
                  },
                  {
                      "lightness": 18
                  }
              ]
          },
          {
              "featureType": "road.local",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#ffffff"
                  },
                  {
                      "lightness": 16
                  }
              ]
          },
          {
              "featureType": "poi",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#f5f5f5"
                  },
                  {
                      "lightness": 21
                  }
              ]
          },
          {
              "featureType": "poi.park",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#dedede"
                  },
                  {
                      "lightness": 21
                  }
              ]
          },
          {
              "elementType": "labels.text.stroke",
              "stylers": [
                  {
                      "visibility": "on"
                  },
                  {
                      "color": "#ffffff"
                  },
                  {
                      "lightness": 16
                  }
              ]
          },
          {
              "elementType": "labels.text.fill",
              "stylers": [
                  {
                      "saturation": 36
                  },
                  {
                      "color": "#333333"
                  },
                  {
                      "lightness": 40
                  }
              ]
          },
          {
              "elementType": "labels.icon",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "transit",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#f2f2f2"
                  },
                  {
                      "lightness": 19
                  }
              ]
          },
          {
              "featureType": "administrative",
              "elementType": "geometry.fill",
              "stylers": [
                  {
                      "color": "#fefefe"
                  },
                  {
                      "lightness": 20
                  }
              ]
          },
          {
              "featureType": "administrative",
              "elementType": "geometry.stroke",
              "stylers": [
                  {
                      "color": "#fefefe"
                  },
                  {
                      "lightness": 17
                  },
                  {
                      "weight": 1.2
                  }
              ]
          }
      ]
    };

    // create map           
    var map = new google.maps.Map($el[0], args);

    // add a markers reference
    map.markers = [];

    // add markers
    $markers.each(function() {

      add_marker($(this), map);

    });

    // center map
    center_map(map);

  }

  /*
   *  add_marker
   *
   *  This function will add a marker to the selected Google Map
   *
   *  @type  function
   *  @date  8/11/2013
   *  @since 4.3.0
   *
   *  @param $marker (jQuery element)
   *  @param map (Google Map object)
   *  @return  n/a
   */

  function add_marker($marker, map) {

    // var
    var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

    // create marker
    var marker = new google.maps.Marker({
      position: latlng,
      map: map,
      icon : 'https://ecor.dk/wp-content/uploads/2019/01/map-marker.png', 
    });

    // add to array
    map.markers.push(marker);

    // if marker contains HTML, add it to an infoWindow
    if ($marker.html()) {
      // create info window
      var infowindow = new google.maps.InfoWindow({
        content: $marker.html()
      });

      // show info window when marker is clicked
      google.maps.event.addListener(marker, 'click', function() {

        infowindow.open(map, marker);

      });
    }

  }

  /*
   *  center_map
   *
   *  This function will center the map, showing all markers attached to this map
   *
   *  @type  function
   *  @date  8/11/2013
   *  @since 4.3.0
   *
   *  @param map (Google Map object)
   *  @return  n/a
   */

  function center_map(map) {

    // vars
    var bounds = new google.maps.LatLngBounds();

    // loop through all markers and create bounds
    $.each(map.markers, function(i, marker) {

      var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

      bounds.extend(latlng);

    });

    // only 1 marker?
    if (map.markers.length == 1) {
      // set center of map
      map.setCenter(bounds.getCenter());
      map.setZoom(16);
    } else {
      // fit to bounds
      map.fitBounds(bounds);
    }

  }

  /*
   *  document ready
   *
   *  This function will render each map when the document is ready (page has loaded)
   *
   *  @type  function
   *  @date  8/11/2013
   *  @since 5.0.0
   *
   *  @param n/a
   *  @return  n/a
   */



  $('.acf-map').each(function() {

    render_map($(this));

  });


  Barba.Pjax.Dom.wrapperId = 'transition__wrapper';
  Barba.Pjax.Dom.containerClass = 'transition__container';

  
  //barba js
  var general = Barba.BaseView.extend({
    namespace: 'general',
    onEnter: function() {
      // The new Container is ready and attached to the DOM.
      console.log('ready');

      
    },
    onEnterCompleted: function() {
      // The Transition has just finished.
      console.log('finished');
      $('.transition__pace').removeClass('is-active');
      $('.transition__overlay').removeClass('is-active');

    },
    onLeave: function() {
      // A new Transition toward a new page has just started.
      console.log('start');
      $('.transition__pace').addClass('is-active');
      $('.transition__overlay').addClass('is-active');
      
    },
    onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.
      console.log('removed');
    }
  });

  general.init();

  Barba.Pjax.start();

  Barba.Prefetch.init(); 

  Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;

  Barba.Pjax.preventCheck = function(evt, element) {
    if (!Barba.Pjax.originalPreventCheck(evt, element)) {
      return false;
    }

    //outcomment if you want to prevent barba when logged in
    // if ($('body').hasClass('logged-in')) { Barba.Pjax.preventCheck = function() { 
    //   return false;  
    // }; } 

    // No need to check for element.href -
    // originalPreventCheck does this for us! (and more!)
    if (/.pdf/.test(element.href.toLowerCase())) {
      return false;
    } else if (/edit/.test(element.href.toLowerCase())) {
      return false;
    } else if (/wp-admin/.test(element.href.toLowerCase())) {
      return false;
    }

    return true;
  };

  var FadeTransition = Barba.BaseTransition.extend({
  start: function() {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the new container
     * (Barba.js also comes with an handy Promise polyfill!)
     */

     

    // As soon the loading is finished and the old page is faded out, let's fade the new page
    Promise
      .all([this.newContainerLoading, this.fadeOut()])
      .then(this.fadeIn.bind(this));
  },

  fadeOut: function() {
    /**
     * this.oldContainer is the HTMLElement of the old Container
     */

    return $(this.oldContainer).animate({ opacity: 1 }, 800 ).promise();
  },

  fadeIn: function() {
    /**
     * this.newContainer is the HTMLElement of the new Container
     * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
     * Please note, newContainer is available just after newContainerLoading is resolved!
     */

    //scroll to top 
    $(window).scrollTop('1px');

    var _this = this;
    var $el = $(this.newContainer);

    $(this.oldContainer).hide();

    $el.css({
      visibility : 'visible',
      opacity : 1
    });

    $el.animate({ opacity: 1 }, 800, function() {
      /**
       * Do not forget to call .done() as soon your transition is finished!
       * .done() will automatically remove from the DOM the old Container
       */

      _this.done();


    });
  }
});

/**
 * Next step, you have to tell Barba to use the new Transition
 */

  Barba.Pjax.getTransition = function() {
    /**
     * Here you can use your own logic!
     * For example you can use different Transition based on the current page or link...
     */

    return FadeTransition;
  };

  Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container, newPageRawHTML) {

    // html head parser borrowed from jquery pjax
    var $newPageHead = $( '<head />' ).html(
        $.parseHTML(
            newPageRawHTML.match(/<head[^>]*>([\s\S.]*)<\/head>/i)[0]
          , document
          , true
        )
    );
    var headTags = [
        "meta[name='keywords']"
      , "meta[name='description']"
      , "meta[property^='og']"
      , "meta[name^='twitter']"
      , "meta[itemprop]"
      , "link[itemprop]"
      , "link[rel='prev']"
      , "link[rel='next']"
      , "link[rel='canonical']"
    ].join(',');
    $( 'head' ).find( headTags ).remove(); // Remove current head tags
    $newPageHead.find( headTags ).appendTo( 'head' ); // Append new tags to the head


    //update body classes
    var response = newPageRawHTML.replace(/(<\/?)body( .+?)?>/gi, '$1notbody$2>', newPageRawHTML)
    var bodyClasses = $(response).filter('notbody').attr('class')
    $('body').attr('class', bodyClasses)


    priceToggle();
    fade();
    toggleMenu();
    fadeMenu();

    //update gmap
    $('.acf-map').each(function() {
        render_map($(this));
    });
    
  });

  Barba.Dispatcher.on('initStateChange', function() {

    fadeMenu();
    toggleMenu();

    $('body').removeClass('is-fixed');
    $('.nav__item').removeClass('is-animated');


    //Update tag manager
    if (typeof ga !== 'function' || Barba.HistoryManager.history.length <= 1) {
      return;
    }

    var trackers = ga.getAll();
    trackers.forEach(function(tracker) {
      ga(tracker.get('name') + '.send', 'pageview', window.location.pathname);
    });

  });


  //menu toggle
  function toggleMenu() {
    $('.nav-toggle').click(function(e) {
      e.preventDefault();
      $('.nav--mobile').toggleClass('is-open');
      $('body').toggleClass('is-fixed');
    });
  }

  toggleMenu();


  //AOS init  
  AOS.init({
    duration: 1200,
    once: true,
    easing: 'ease'
  })


  //fancybox
  $('[data-fancybox]').fancybox({ 
    toolbar  : false,
    smallBtn : true,
    iframe : { 
      preload : false 
    }
  });


  //toggle button & prices - make this less redundant someday when your brain function properly
  function priceToggle() {
    $('.btn--toggle').click(function() {
      var mainParent = $(this).parent('.pricetabel__btn');
      if($(mainParent).find('input.btn--toggle').is(':checked')) {
        $(this).addClass('is-active');
        $('.pricetabel__price--proanno').addClass('is-active');
        $('.pricetabel__price--monthly').removeClass('is-active');
        $('.pricetabel__price--monthly').addClass('is-hidden');
      } else {
        $(this).removeClass('is-active');
        $('.pricetabel__price--monthly').addClass('is-active');
        $('.pricetabel__price--proanno').removeClass('is-active');
      }

    }); 
  }

  priceToggle();


  // fade in employees
  function fade() {
    $('.employee__item').each(function(index){
      var delayNumber = index * 100;
      $(this).attr('data-aos-delay', delayNumber).data('delayNumber');
    })
  }

  fade();


  //fade in menu-items

  function fadeMenu() {
    $('.nav-toggle').click(function() {
      $('.nav__item').each(function(index){        
          var delayNumber = index * 100;
          
          $(this).delay(delayNumber).queue(function(next) {
            $(this).toggleClass('is-animated');
            next();
          });  

      })
    }); 
  }

  fadeMenu();

});