<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}


	$img = get_field('page_img');
	$text = get_field('page_text');
	$link = get_field('page_link');
	$video = get_field('page_video');
	$video_link = get_field('page_video_link');
	$link_text = get_field('page_link_text');
	$cta = get_field('page_cta');

	//trim url for easier string replacement
	$trim_url = parse_url($video_link, PHP_URL_PATH);

	//replace and strip string from videolink variable down to video ID for thumbnail use
	$video_id = str_replace('/embed/', '', $trim_url);
?>

<section class="page__hero padding--both" data-aos="width">
	<div class="wrap hpad">
		<div class="row">
			<div class="col-sm-6 page__intro">
				<h1 data-aos="fade-up" class="page__title"><?php echo esc_html($title); ?></h1>
				<?php echo $text; ?>
				<?php if ($link && $link_text) : ?>
				<a class="btn btn--green page__btn" href="<?php echo esc_url($link); ?>"><?php echo esc_html($link_text); ?></a>
				<br>
				<?php endif; ?>
				<?php if ($cta === true) : ?>
				<a class="btn btn--gray-light" href="mailto:kh@ecor.dk?subject=Jeg ønsker at blive ringet op til en uforpligtende snak">Book et møde</a>
				<?php endif; ?>
			</div>

			<?php if ($img) : ?>
			<div class="col-sm-6 page__img" data-aos="fade-up">
				<img src="<?php echo esc_url($img['sizes']['large']); ?>" alt="<?php echo $img['alt']; ?>">
			</div>
			<?php endif; ?>

			<?php if ($video === true ) : ?>
			<div class="col-sm-6 page__img page__video" data-aos="fade-up">
				<div class="embed embed__item embed--16-9">
				<a class="page__video--link" style="background-image: url(https://img.youtube.com/vi/<?php echo esc_html($video_id); ?>/maxresdefault.jpg);" data-fancybox data-type="iframe" data-src="<?php echo esc_url($video_link); ?>?autoplay=1&showinfo=0&controls=0&rel=0" href="javascript:;"><i class="fas fa-play page__video--play"></i></a>
				</div>		
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>
