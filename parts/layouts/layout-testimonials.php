<?php 
/**
* Description: Lionlab testimonial field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

//field group
$img = get_sub_field('testimonial_img');
$title = get_sub_field('testimonial_title');
$text = get_sub_field('testimonial_text');
?>

<section class="testimonial padding--<?php echo $margin; ?>" style="background-image: url(<?php echo esc_url($img['url']); ?>);">
  <div class="wrap hpad">
    <div class="row">
      <div class="col-sm-8 col-sm-offset-2 center testimonial__item">
        <h2 class="testimonial__title"><?php echo esc_html($title); ?></h2>
        <em class="testimonial__quote"><?php echo esc_html($text); ?></em>
      </div>
    </div>
  </div>
</section>
