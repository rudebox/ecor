<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="404 padding--both">
  	<div class="wrap hpad clearfix">

    <h2>Ups!</h2>
    <p>Siden du søgte efter kunne ikke findes.</p>

    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>