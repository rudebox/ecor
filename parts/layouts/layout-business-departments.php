<?php 
/**
* Description: Lionlab business departments repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');
$sub_title = get_sub_field('subheader');

if (have_rows('business_box') ) :
?>

<section class="business-department <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
			<h2 class="business-department__header center"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<?php if ($sub_title) : ?>
			<p class="business-department__header--subheader center"><?php echo esc_html($sub_title); ?></p>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('business_box') ) : the_row(); 
				$title = get_sub_field('business_title');
				$img = get_sub_field('business_img');
				$link = get_sub_field('business_link');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-4 business-department__item">
				
				<div class="business-department__img">
					<img src="<?php echo esc_url($img['sizes']['business']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				</div>
				
				<h3 class="business-department__title"><?php echo esc_html($title); ?></h3>
				<div class="business-department__btn"><span>Læs mere</span> <?php echo file_get_contents('wp-content/themes/ecor/assets/img/arrow.svg'); ?></div>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>