<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
			<h2 class="link-boxes__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$link = get_sub_field('link');
				$icon = get_sub_field('icon');

				$image_id = $icon['ID'];
			?>

			<div class="col-sm-6 link-boxes__item">
				<?php if ($icon) : ?>
					<div class="link-boxes__icon" data-aos="zoom-in">
						<?php echo zen_inline_if_svg( $image_id, 'url' ); ?>
					</div>
				<?php endif; ?>
				<h3><?php echo esc_html($title); ?></h3>
				<?php echo $text; ?>
				<a href="<?php echo esc_url($link); ?>" class="link-boxes__btn"><span>Læs mere</span> <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arrow.png"></a> 
			</div> 
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>