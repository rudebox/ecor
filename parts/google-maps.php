<?php 
/**
* Description: Lionlab google maps repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

?>

<section>

<?php if ( have_rows('locations') ) : ?>

	<div class="acf-map">

		<?php
		// Loop Google Maps
		while ( have_rows('locations') ) : the_row();
			$location = get_sub_field('google_maps');
		?>

			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>

		<?php endwhile; ?>

	</div>

<?php endif; ?>


</section>