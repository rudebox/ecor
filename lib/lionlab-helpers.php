<?php
 /**
   * Description: Lionlab helpers
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

	//remove countrycode from phone
	function get_formatted_phone($str) {
	  
	  // Remove +45
	  $str = str_replace('+', '00', $str);

	  // Only allow integers
	  $str = preg_replace('/[^0-9]/s', '', $str);

	  return $str;
	}


	//allow svg uploads
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types'); 


	//Allow for SVG images via ACF
	//Use:
	//$acf_image = get_field( 'field_name' );
	//$image_id = $acf_image['ID']; // assumes image is saved as array in ACF
	//echo zen_inline_if_svg( $image_id, 'size' ); // if size not set, will default to medium

	function zen_inline_if_svg( $att_id, $size = 'medium' ){

	    if ( !$att_id ) {
	        return '';
	    }

	    $mime = get_post_mime_type( $att_id );

	    if ($mime && $mime === 'image/svg+xml'){

	        $svg = wp_get_attachment_url( $att_id );

	        return file_get_contents($svg, false, stream_context_create(
	          array(
	            'ssl' => array(
	              'verify_peer' => false,
	              'verify_peer_name' => false
	            )
	          )
	        )
	      );
	    }
	    return wp_get_attachment_image( $att_id, $size );
	}



	// Custom excerpt length
	function custom_excerpt_length( $length ) {
	  return 20;
	}

	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


	// Custom excerpt text
	function custom_excerpt_more( $more ) {
	  return '&hellip;';
	}

	add_filter('excerpt_more', 'custom_excerpt_more');


	// Move Yoast to bottom
	function yoasttobottom() {
	  return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


	//get proper title
	function get_proper_title( $id = null  ) {
	  
	  if ( is_null($id) ) {
	    global $post;
	    $id = $post->ID;
	  }

	  $acf_title = get_field('page_titel', $id);

	  return ($acf_title) ? $acf_title : get_the_title( $id );
	}

?>